<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE
    section
    PUBLIC "-//OASIS//DTD DocBook XML 5.0//EN"
    "http://www.oasis-open.org/docbook/xml/5.0/docbook.dtd"
>

<section
    xml:id="OpenSSL"
>
    <title>OpenSSL</title>

    <para>
        The OpenSSL package contains libraries and executables for handling
        TLS.  It also contains a default store of certificates.  Many
        third-party executables will leverage the OpenSSL library and its store
        of certificates.  But an executable may use its own TLS library and
        store of certificates.  Each executable that uses its own TLS framework
        may need to be configured separately.
    </para>

    <para>
        Resources:
    </para>

    <itemizedlist>
        <listitem>
            <link xlink:href="https://wiki.openssl.org/index.php/Command_Line_Utilities"/>
        </listitem>
        <listitem>
            <link xlink:href="http://gagravarr.org/writing/openssl-certs/others.shtml"/>
        </listitem>
    </itemizedlist>

    <section
        xml:id="Identify_Libraries_Dynamically_Linked_to_an_Executable"
    >
        <title>Identify Libraries Dynamically Linked to an Executable</title>

        <para>
            To know whether a binary executable uses a given OpenSSL library on
            a machine, you can execute a command to see to which libraries a
            given executable is dynamically linked.
        </para>

        <para>
            On a GNU/Linux machine, to determine to which libraries a given
            executable is dynamically linked, execute the command below,
            replacing <replaceable>EXECUTABLE</replaceable> with the path of an
            executable.
        </para>

        <programlisting
            language="bourne"
        >ldd <replaceable>EXECUTABLE</replaceable></programlisting>

        <para>
            On a Mac OS machine, to determine to which libraries a given
            executable is dynamically linked, execute the command below,
            replacing <replaceable>EXECUTABLE</replaceable> with the path of an
            executable.
        </para>

        <programlisting
            language="bourne"
        >otool -L <replaceable>EXECUTABLE</replaceable></programlisting>

    </section>

    <section
        xml:id="Identify_the_OpenSSL_Configuration_Directory"
    >
        <title>Identify the OpenSSL Configuration Directory</title>

        <para>
            To determine under which directory a given OpenSSL library stores
            its configuration (in file <filename>openssl.cnf</filename>) and
            certificate store (in directory <filename
                class="directory">certs</filename>), execute the command
            below.  If there are multiple versions of OpenSSL on the machine,
            you may need to provide the full path to a given
            <filename>openssl</filename> executable.
        </para>

        <programlisting
            language="bourne"
        ><![CDATA[openssl version -d]]></programlisting>

        <para>
            On a Debian 9 machine, for the
            <filename>/usr/bin/openssl</filename> executable, this command
            yields directory <filename
                class="directory">/usr/lib/ssl</filename>.  <filename
                class="symlink">/usr/lib/ssl/certs</filename> is a symbolic
            link to directory <filename
                class="directory">/etc/ssl/certs</filename>.  <filename
                class="symlink">/usr/lib/ssl/openssl.cnf</filename> is a
            symbolic link to file <filename>/etc/ssl/openssl.cnf</filename>.
            And <filename class="symlink">/usr/lib/ssl/private</filename> is a
            symbolic link to directory <filename
                class="directory">/etc/ssl/private</filename>.
        </para>

        <para>
            On a Mac OS machine with the Homebrew package manager, for the
            <filename>/usr/local/bin/openssl</filename> executable, this
            command yields directory <filename
                class="directory">/usr/local/etc/openssl</filename>.
        </para>

    </section>

    <section
        xml:id="Read_a_Certificate_Using_OpenSSL"
    >
        <title>Read a Certificate Using OpenSSL</title>

        <para>
            To print information about a given certificate to the console,
            execute the command below, replacing
            <replaceable>CA_CERT</replaceable> with the filename of a given CA
            certificate (e.g.,
            <filename>Acme_Incorporated_Cert.pem</filename>).
        </para>

        <programlisting
            language="bourne"
        ><![CDATA[CA_CERT=']]><replaceable>CA_CERT</replaceable><![CDATA['

openssl \
    x509 \
    -text \
    -noout \
    -in "${CA_CERT}"]]></programlisting>

    </section>

    <section
        xml:id="Add_a_CA_Certificate_to_an_OpenSSL_Store"
    >
        <title>Add a CA Certificate to an OpenSSL Store</title>

        <section
            xml:id="Add_a_CA_Certificate_to_an_OpenSSL_Store_-_Debian"
        >
            <title>Add a CA Certificate to an OpenSSL Store - Debian</title>

            <para>
                On a Debian 9 machine, to add a CA certificate to the OpenSSL
                store, execute the commands below, replacing
                <replaceable>CA_CERT</replaceable> with the filename of a given
                CA certificate (e.g.,
                <filename>Acme_Incorporated_Cert.pem</filename>).  Note that it
                is a requirement that files in the <filename
                    class="directory">/usr/share/local/share/ca-certificates</filename>
                directory have a <filename class="extension">.crt</filename>
                file extension in order to be implicitly trusted.  Note that a
                <option>-f</option> or <option>--fresh</option> option to
                command <command>update-ca-certificates</command> will
                completely refresh symbolic links under directory <filename
                    class="directory">/etc/ssl/certs</filename>.
            </para>

            <programlisting
                language="bourne"
            ><![CDATA[CA_CERT=']]><replaceable>CA_CERT</replaceable><![CDATA['

CA_CERT_CRT="$(basename -- "${CA_CERT}" .pem).crt"

sudo \
    -- \
    install \
    --mode u=rwX,go=rX  \
    -- \
    "${CA_CERT}" \
    "/usr/local/share/ca-certificates/${CA_CERT_CRT}"

sudo update-ca-certificates]]></programlisting>

        </section>

        <section
            xml:id="Add_a_CA_Certificate_to_an_OpenSSL_Store_-_Mac_OS_Homebrew"
        >
            <title>Add a CA Certificate to an OpenSSL Store - Mac OS Homebrew</title>

            <para>
                On a Mac OS machine with the Homebrew package manager, to add a
                CA certificate to the OpenSSL store, execute the commands
                below, replacing <replaceable>CA_CERT</replaceable> with the
                filename of a given CA certificate (e.g.,
                <filename>Acme_Incorporated_Cert.pem</filename>).  Note that
                this is a very generic way to add a CA certificate to an
                OpenSSL certificate store.  Note that it is customary for
                Homebrew to be installed and managed by a non-root user.
            </para>

            <programlisting
                language="bourne"
            ><![CDATA[CA_CERT=']]><replaceable>CA_CERT</replaceable><![CDATA['

CERTS_DIR="$(
    openssl \
        version \
        -d |
    sed 's/^OPENSSLDIR: "//;s/"$//'
)/certs"

CA_CERT_HASH="$(
    openssl \
        x509 \
        -hash \
        -noout \
        -in "${CA_CERT}"
)"

install \
    --mode u=rwX,go=rX  \
    -- \
    "${CA_CERT}" \
    "${CERTS_DIR}/${CA_CERT}"

ln \
        -sf \
        "${CA_CERT}" \
        "${CERTS_DIR}/${CA_CERT_HASH}.0"]]></programlisting>

        </section>

    </section>

</section>
